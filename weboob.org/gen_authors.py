#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
from woob.browser import DomainBrowser
from woob.browser.pages import HTMLPage
import logging
logging.basicConfig(level=logging.DEBUG)


OHLOH_URL = 'https://www.openhub.net/p/weboob/contributors?sort=commits'

def get_entries():
    companies = [
        {'link': 'https://www.budget-insight.com/', 'icon': '/media/images/bi.png', 'pos': 0, 'name': 'Budget Insight'},
        {'link': 'http://www.winancial.com/', 'icon': '/media/images/winancial.png', 'pos': 1, 'name': 'Winancial'},
        {'link': 'https://cozy.io/', 'icon': '/media/images/cozycloud.png', 'pos': 2, 'name': 'Cozy Cloud'},
        {'link': 'https://tech.geneanet.org/', 'icon': '/media/images/geneanet.png', 'pos': 3, 'name': 'Geneanet'},
    ]
    entries = {'org': [], 'people': []}
    b = DomainBrowser()
    b.TIMEOUT = 600

    url = OHLOH_URL
    while url is not None:
        page = HTMLPage(b, b.location(url))

        for tr in page.doc.getroot().cssselect('table.table tbody tr'):
            a = tr.find('td').findall('a')[-1]
            url = a.attrib['href']
            name = a.text.strip()
            commits = int(tr.findall('td')[3].text.strip())
            #userdoc = b.location(url)
            #commits = int(userdoc.xpath('//div[@class="committer-summary"]/div[@class="span2"]/span[@class="float_right"]')[0].text.strip())
            icon = tr.find('td').find('a').find('img').attrib['src']
            entry = {'name':    name,
                     'commits': commits,
                     'icon':    b.absurl(icon),
                     'link':    'https://www.openhub.net%s' % url,
                    }
            entries['people'].append(entry)

        li = page.doc.getroot().cssselect('div#contributions_index_page ul.pagination li.next')
        if len(li) > 0 and not 'disabled' in li[0].attrib.get('class'):
            url = li[0].find('a').attrib['href']
        else:
            url = None

    for i, entry in enumerate(entries['people']):
        #entry['side'] = 'left' if (i < round(len(entries['people'])/2.0)) else 'right'
        entry['side'] = 'left' if not (i%2) else 'right'

    for i, entry in enumerate(companies):
        entry['side'] = 'left' if not (i%2) else 'right'
        entries['org'].append(entry)

    return entries


entries = get_entries()
with open('authors.yaml', 'w') as fp:
    yaml.dump(dict(entries=entries), fp)
