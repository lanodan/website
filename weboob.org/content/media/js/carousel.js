$(document).ready(function() {
  setupCarousel();
});

var carousel_delay = 6000;

function setupCarousel() {
  // So that last pane is first pane's clone
  $('#infinite-carousel .panes').append(
    $('#infinite-carousel .panes .pane:first-child').clone()
  );

  $('.prev').click(function() { slide(-1, true); });
  $('.next').click(function() { slide(1, true); });

  slideIndex = 0;
  slideWidth = $('#infinite-carousel').width();
  slidesNumber = $('#infinite-carousel ul li').size();

  $(window).resize(function() {
    slideWidth = $('#infinite-carousel').width();
    $('#infinite-carousel').addClass('noAnimation');
    var margin = slideIndex * slideWidth * -1;
    $('#infinite-carousel ul').css('margin-left', margin+'px');
  });

  $('#infinite-carousel .panes').css('width', 100*slidesNumber+'%');
  $('#infinite-carousel .panes').css('max-width', 100*slidesNumber+'%');
  $('#infinite-carousel .panes .pane').css('max-width', 100/slidesNumber+'%');

  rotation = setTimeout(function() {slide(0, true);}, carousel_delay);

  for (i=0; i < Math.floor(Math.random()*slidesNumber); i++)
  {
    slide(1, false);
  }
}

function slide(direction, animate) {
  clearTimeout(rotation);
  if (direction == 0) {
    if ($('#infinite-carousel:hover, .prev:hover, .next:hover').size()) {
      rotation = setTimeout(function() {slide(0);}, carousel_delay);
      return;
    } else {
      direction = 1;
    }
  }
  slideIndex += direction;

  if (slideIndex >= 0 && slideIndex < slidesNumber) {
    var margin = slideIndex * slideWidth * -1;
    if (animate) {
      $('#infinite-carousel').removeClass('noAnimation');
      if (Modernizr.csstransitions) {
        $('#infinite-carousel ul').css('margin-left', margin+'px');
      } else {
        $('#infinite-carousel ul').animate({
          marginLeft: margin+'px'
        }, 'slow');
      }
    }
    else
    {
      $('#infinite-carousel').addClass('noAnimation');
      $('#infinite-carousel ul').css('margin-left', margin+'px');
      $('#infinite-carousel').removeClass('noAnimation');
    }

    rotation = setTimeout(function() {slide(0, true);}, carousel_delay);
  } else if (slideIndex >= slidesNumber) {
    slideIndex = 0;
    var margin = slideIndex * slideWidth * -1;
    $('#infinite-carousel').addClass('noAnimation');
    $('#infinite-carousel ul').css('margin-left', margin+'px');
    setTimeout(function() {slide(1, true);}, 100);
  } else {
    slideIndex = slidesNumber - 1;
    var margin = slideIndex * slideWidth * -1;
    $('#infinite-carousel').addClass('noAnimation');
    $('#infinite-carousel ul').css('margin-left', margin+'px');
    setTimeout(function() {slide(-1, true);}, 100);
  }
}
