jQuery(document).ready(function(j) {
    j("time.timeago").timeago();

    j("#modules-list div.details").hide();

    j("#modules-list li.module").bind("click", function(event) {
        var target = j(event.target);

        var is_link = typeof(target.attr("href")) != "undefined";
        if (is_link) {
            is_external = target.attr("href").indexOf("http") == 0;
        }
        else {
            is_external = false;
        }

        if (!is_external) {
            var parent = target.parents("li.module")[0];
            j(parent).find('.hideshow').trigger('hideshow');
        }
    });

    j("#hideshow, #modules-list .hideshow").show();

    j("#hideshow").bind("click", function(event) {
        event.preventDefault();
        var target = j(event.target);
        if (target.hasClass("activated")) {
            j("#modules-list .hideshow").trigger("hide");
            target.text("Display all module details");
            target.removeClass("activated");
        }
        else {
            j("#modules-list .hideshow").trigger("show");
            target.text("Hide all module details");
            target.addClass("activated");
        }
    });

    j("#modules-list .hideshow").bind("click", function(event) {
        event.preventDefault();
        event.stopPropagation();
        j(event.target).trigger("hideshow");
    });

    j("#modules-list .hideshow").bind("hideshow", function(event) {
        var target = j(event.target);
        if (target.hasClass("activated")) {
            target.trigger("hide");
        }
        else {
            target.trigger("show");
        }
    });

    j("#modules-list .hideshow").bind("hide", function(event) {
        var target = j(event.target);
        var parent = target.parents("li.module")[0];
        j(parent).find("div.details").slideUp();
        target.text("Show details");
        target.removeClass("activated");
    });

    j("#modules-list .hideshow").bind("show", function(event) {
        var target = j(event.target);
        var parent = target.parents("li.module")[0];
        j(parent).find("div.details").slideDown();
        target.text("Hide details");
        target.addClass("activated");
    });
});
