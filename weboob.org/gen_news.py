#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from datetime import datetime
from dateutil.parser import parse as parse_date
import feedparser
import yaml

FEED_URL = 'https://git.weboob.org/weboob/weboob/tags?feed_token=Uzxrp-vMMgUTJ8TpuMLc&format=atom'


def clean_content(content):
    return re.sub('href="/', 'href="https://git.weboob.org', content)


def get_entries():
    feed = feedparser.parse(FEED_URL)
    entries = list()
    for feed_entry in feed.entries:
        if not hasattr(feed_entry, 'updated'):
            continue
        dt = parse_date(feed_entry.updated)
        entry = { \
            'title': 'Weboob v%s released' % feed_entry.title,
            'updated': dt,
            'content': clean_content(feed_entry.content[0].value),
            'author': feed_entry.author_detail.name,
            'link': feed_entry.link}
        entries.append(entry)
    return entries

entries = [
    {
        'title': 'Weboob will become woob',
        'updated': datetime(2021, 2, 21, 10, 24, 37),
        'content': """
<p>
When weboob was started in 2010, 11 years ago, the name was chosen, without a hidden agenda, since as a French speaker, "boob" wasn't part of my vocabulary.
</p><p>
Following its release and the ensuing reactions, during its first years, the project was complemented with various provocative elements (icons, application names, English slurs in the code). This was done with the sole motive that at that time, it was seen as "fun".
</p><p>
But in practice, it's been years the project isn't following this approach anymore, it's used as an essential building block of professional companies, the provocative elements are progressively removed, and the professionnalisation question is being raised.
</p><p>
Recently, a notorious weboob contributor has ended his own life and it was later discovered he had an affinity for far-right pro-conspiracy groups.
</p><p>
The weboob team do not recognize themselves in this extreme ideology, from which they are very far. It was decided to finally remove remaining items which were irrelevant to the technical qualities of the weboob project, and which harm the visibility it deserves.
</p><p>
This raised once more the issue of the project name, which gained some notoriety, but is still heavily associated to the poor taste jokes typical from its early years.
</p><p>
This is why it was decided to adopt a neutral and more professional position. To start again with a clean slate and mark a real turning point, it was also decided to rename weboob. The chosen name is "woob", since the project could have been named that way in the first place, and as it's close to the current name, it's possible to link the two names and still keep the original motto "Web Outside Of Browsers".
</p><p>
Those changes will be done in the weeks following this announcement, and after them a new version will be released.
</p>
""",
        'author': 'Romain Bignon',
        'link': 'https://lists.symlink.me/pipermail/weboob/2021-February/001646.html',
    }
]

entries += get_entries()[0:3]
with open('news.yaml', 'w') as fp:
    yaml.dump(dict(entries=entries), fp)
