#!/usr/bin/env python3

import re
import gzip
import os
import sys
import posixpath
from random import randint
from datetime import datetime
import requests
import yaml

from woob.core.repositories import Repository
from woob.browser import Browser

REPO_URL = 'https://updates.woob.tech/%s/main/'

if '--devel' in sys.argv:
    version = 'unstable'
else:
    version = 'stable'

REPO_URL = REPO_URL % version

if '--fake' in sys.argv:
    fake = True
else:
    fake = False


def get_installs():
    if fake:
        return None

    installed = {}
    LOG_REGEXP = re.compile(r'([\w\.]+ )?([\w:\.]+) - - \[[^\]]+\] \"(\w+) /([\w+\.]+)/(\w+)/(\w+)\.tar\.gz (.*)(Firefox/3.6.13|(weboob|woob)/[\w\.]+)"$')
    for directory in ('nginx', 'apache2'):
        for f in os.listdir(directory):
            if not f.startswith('weboob-updates'):
                continue

            if f.endswith('.gz'):
                func = gzip.open
            else:
                func = open

            with func(os.path.join(directory, f), 'rt') as fp:
                for line in fp.readlines():
                    m = LOG_REGEXP.match(line.strip())
                    if not m:
                        continue

                    ip = m.group(2)
                    #version = m.group(4)
                    #repository = m.group(5)
                    name = m.group(6)
                    #useragent = m.group(8)

                    if name in installed:
                        installed[name].add(ip)
                    else:
                        installed[name] = set([ip])

    return installed


def url_exists(url):
    print(url)
    try:
        r = requests.head(url, timeout=5)
    except requests.exceptions.ReadTimeout:
        return False
    else:
        return r.status_code != 404


def count_installs(installed, module_name):
    if fake:
        return randint(1, 42)
    else:
        return len(installed.get(module_name, []))


def get_modules(installed):
    repo = Repository(REPO_URL)
    repo.retrieve_index(Browser(), None)  # TODO retrieve_index should accept None
    mods = dict()

    for name, module in sorted(repo.modules.items()):
        m = re.match('(.*) <(.*)>', module.maintainer)
        if m:
            maintainer = m.group(1)
            email = m.group(2)
        else:
            maintainer = email = module.maintainer

        if not module.icon:
            module.icon = posixpath.join(REPO_URL, module.name + '.png')
        if not url_exists(module.icon):
            module.icon = None
        updated = datetime.strptime(str(module.version), '%Y%m%d%H%M')
        mods[module.name] = { \
                    'name': module.name,
                    'updated': updated,
                    'description': module.description,
                    'maintainer': maintainer,
                    'email': email,
                    'capabilities': [x[3:] for x in module.capabilities if x.startswith('Cap')],
                    'installed': count_installs(installed, module.name),
                    'icon': module.icon,
        }
    return mods


def get_popular_modules(modules):
    pop = list()
    for module in sorted(modules.values(), key=lambda m: m['installed'], reverse=True):
        if module['icon']:
            pop.append(module['name'])
        else:
            print('Warning: module %s is popular but does not have an icon!' % module['name'], file=sys.stderr)

        if len(pop) >= 30:
            break

    return pop


installed = get_installs()
modules = get_modules(installed)
popular_modules = get_popular_modules(modules)


with open('modules.yaml', 'w') as fp:
    yaml.dump(dict(
        module_names=sorted(modules.keys()),
        modules=modules,
        popular_modules=popular_modules,
        repo_url=REPO_URL), fp)
