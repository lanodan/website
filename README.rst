Requirements
------------
`Hyde <http://hyde.github.com/>`_ version 0.8.4 or more.


Local testing of the website
----------------------------

You have first to build the auto-generated data (only for ``weboob.org``)::

    make GEN_MODULES_OPTS=--fake update

To use the unstable version of repositories, use::

    make GEN_MODULES_OPTS="--fake --devel" update

The module installation count will be randomly generated.

Then you can generate the static HTML::

    make clean gen

Or you can use the internal web server of Hyde::

    make serve

When in doubt, run ``make clean gen`` to force regeneration, and restart the web server.
``make serve`` actually runs ``make clean gen`` before, so you can just restart the web
server with make serve.


Production
----------

Run ``make update``.
You need access to the server logs for the module installation count.
